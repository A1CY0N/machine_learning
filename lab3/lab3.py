#!/usr/bin/env python
import numpy as np
import pandas as pd

from common import describe_data, test_env, classification_metrics
import common.describe_data as describe_data
import common.classification_metrics as classification_metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import ComplementNB, MultinomialNB, BernoulliNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.preprocessing import Imputer

__author__ = 'A1CY0N'
__version__ = '0.1-dev'
__date__ = '14 november 2019'


def read_data(file):
    """Return pandas dataFrame read from xls file"""
    try:
        return pd.read_excel(file)
    except FileNotFoundError:
        exit('ERROR: ' + file + ' not found')


def preprocess_data(df, verbose=False):
    y_column = 'In university after 4 semesters'

    # Features can be excluded by adding column name to list
    drop_columns = []

    categorical_columns = [
        'Faculty',
        'Paid tuition',
        'Study load',
        'Previous school level',
        'Previous school study language',
        'Recognition',
        'Study language',
        'Foreign student'
    ]

    other_columns = ["Estonian language exam points",
                     "Estonian as second language exam points",
                     "Mother tongue exam points",
                     "Narrow mathematics exam points",
                     "Wide mathematics exam points",
                     "Mathematics exam points"
                     ]

    # Handle dependent variable
    if verbose:
        print('Missing y values: ', df[y_column].isna().sum())

    y = df[y_column].values
    # Encode y. Naive solution
    y = np.where(y == 'No', 0, y)
    y = np.where(y == 'Yes', 1, y)
    y = y.astype(float)

    # Drop also dependent variable variable column to leave only features
    drop_columns.append(y_column)
    df = df.drop(labels=drop_columns, axis=1)

    # Remove drop columns for categorical columns just in case
    categorical_columns = [
        i for i in categorical_columns if i not in drop_columns]


    # Handle missing data. At this point only exam points should be missing
    # It seems to be easier to fill whole data frame as only particular columns
    """
    imputer = Imputer(missing_values="NaN", strategy="mean", axis = 0)
    imputer = imputer.fit(df)
    df = imputer.transform(df)
    df = pd.DataFrame.from_records(df)
    """
    for column in categorical_columns:
        df[column] = df[column].fillna(value='Missing')
    for column in categorical_columns:
        df = pd.get_dummies(df, prefix=[column], columns=[column])
    for column in other_columns:
        df[column] = df[column].fillna(value=0)

    if verbose:
        describe_data.print_nan_counts(df)

    if verbose:
        describe_data.print_nan_counts(df)

    # Return features data frame and dependent variable
    return df, y


# STUDENT SHALL CREATE FUNCTIONS FOR LOGISTIC REGRESSION CLASSIFIER, KNN
# CLASSIFIER, SVM CLASSIFIER, NAIVE BAYES CLASSIFIER, DECISION TREE
# CLASSIFIER AND RANDOM FOREST CLASSIFIER
def logistic_regression_classifier(X, y, print_text='Logistic regression classifier'):

    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    # Scale features
    clf = LogisticRegression(
        random_state=0, solver='newton-cg', multi_class='multinomial')
    clf.fit(X_train, y_train)
    classification_metrics.print_metrics(
        y_test, clf.predict(X_test), print_text, verbose=True)
    return clf


def knn_classifier(X, y, print_text='KNN classifier'):

    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    # Scale features
    clf = KNeighborsClassifier(n_neighbors=8, metric='minkowski', p=2)
    clf.fit(X_train, y_train)
    classification_metrics.print_metrics(
        y_test, clf.predict(X_test), print_text, verbose=True)
    return clf


def svm_classifier(X, y, print_text='SVM classifier'):

    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    # Scale features
    clf = SVC(kernel='sigmoid', gamma=1, tol=0.1,
              probability=True)
    clf.fit(X_train, y_train)
    classification_metrics.print_metrics(
        y_test, clf.predict(X_test), print_text, verbose=True)
    return clf


def naive_bayes_classifier(X, y, print_text='Naive bayes classifier'):

    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    # Scale features
    clf = BernoulliNB()
    clf.fit(X_train, y_train)
    classification_metrics.print_metrics(
        y_test, clf.predict(X_test), print_text, verbose=True)
    return clf


def decision_tree_classifier(X, y, print_text='Decision tree classifier'):

    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    # Scale features
    clf = DecisionTreeClassifier(random_state=0)
    clf.fit(X_train, y_train)
    classification_metrics.print_metrics(
        y_test, clf.predict(X_test), print_text, verbose=True)
    return clf


def random_forest_classifier(X, y, print_text='Random forest classifier'):

    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    # Scale features
    clf = RandomForestClassifier(n_estimators=100, random_state=0)
    clf.fit(X_train, y_train)
    classification_metrics.print_metrics(
        y_test, clf.predict(X_test), print_text, verbose=True)
    return clf


if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn']
    test_env.versions(modules)

    students = read_data('data/students.xlsx')

    # STUDENT SHALL CALL PRINT_OVERVIEW AND PRINT_CATEGORICAL FUNCTIONS WITH
    # FILE NAME AS ARGUMENT

    students_X, students_y = preprocess_data(students)

    describe_data.print_overview(
        students, file='results/students_overview.txt')
    describe_data.print_categorical(
        students, file='results/students_categorical_data.txt')

    students_not_serious = students[(
        students['In university after 4 semesters'] == 'No')]

    describe_data.print_overview(
        students_not_serious, file='results/students_not_serious_overview.txt')
    describe_data.print_categorical(
        students_not_serious, file='results/students_not_serious_categorical_data.txt')

    # describe_data.print_overview(students_X)
    # describe_data.print_categorical(students_X)

    # STUDENT SHALL CALL CREATED CLASSIFIERS FUNCTIONS
    logistic_regression_classifier(students_X, students_y)
    knn_classifier(students_X, students_y)
    svm_classifier(students_X, students_y)
    naive_bayes_classifier(students_X, students_y)
    decision_tree_classifier(students_X, students_y)
    random_forest_classifier(students_X, students_y)

    print('Done')
