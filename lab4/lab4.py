#!/usr/bin/env python
"""lab1 template"""
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
import common.test_env as test_env
import common.describe_data as describe_data
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

__author__ = 'A1CY0N'
__version__ = '0.1-dev'
__date__ = '8 december 2019'


def read_data(file):
    """Return pandas dataFrame read from csv file"""
    try:
        return pd.read_csv(file, sep=',', decimal='.', encoding='latin-1')
    except FileNotFoundError:
        exit('ERROR: ' + file + ' not found')


def preprocess_data(df):
    df = df.drop(labels=["CUST_ID"], axis=1)
    for column in df:
        df[column] = df[column].fillna(value=df[column].mean())
    return df


def select_cluster(data):
    sse = {}
    for k in range(1, 12):
        print(k)
        kmeans = KMeans(n_clusters=k, max_iter=1000).fit(data)
        data["clusters"] = kmeans.labels_
        # print(data["clusters"])
        # Inertia: Sum of distances of samples to their closest cluster center
        sse[k] = kmeans.inertia_
    return sse


def sse_visualisation(sse):
    plt.figure()
    plt.plot(list(sse.keys()), list(sse.values()))
    plt.xlabel("Number of cluster")
    plt.ylabel("SSE")
    plt.savefig('results/SSE.png', papertype='a4')


def clusters_visualisation(X, y, name):
    colors = ['#faf3dd', '#c8d5b9', '#8fc0a9', '#68b0ab', '#4a7c59',
              '#034732', '#008148', '#c6c013', '#ef8a17', '#ef2917']
    markers = ['o', 'X', 's', 'D']
    color_idx = 0
    marker_idx = 0

    plt.figure(name)

    for cluster in range(0, len(set(y))):
        plt.scatter(X[y == cluster, 0], X[y == cluster, 1],
                    s=5, c=colors[color_idx], marker=markers[marker_idx])
        color_idx = 0 if color_idx == (len(colors) - 1) else color_idx + 1
        marker_idx = 0 if marker_idx == (len(markers) - 1) else marker_idx + 1

    plt.title(name)
    plt.savefig('results/' + name + '.png', papertype='a4')
    plt.show()


if __name__ == '__main__':
    REQUIRED = ['matplotlib', 'pandas']
    test_env.versions(REQUIRED)
    df = read_data('data/cc_general.csv')
    print("\nKeys of df dataset:")
    print(df.keys())
    print("\nNumber of rows and columns of df dataset:")
    print(df.shape)
    data = preprocess_data(df)
    sse = select_cluster(data)
    sse_visualisation(sse)
    describe_data.print_overview(
        df, file='results/cc_general_overview.txt')
    describe_data.print_categorical(
        df, file='results/cc_general_categorical_data.txt')
    X_tsne = TSNE(n_components=2, random_state=0).fit_transform(data)
    clusters_visualisation(X_tsne, np.full(X_tsne.shape[0], 0),
                           'T-SNE without clusters')
    k_means = KMeans(n_clusters=10, init='k-means++', random_state=42)
    y_kmeans = k_means.fit_predict(data)
    clusters_visualisation(X_tsne, y_kmeans,
                           'T-SNE with clusters')
