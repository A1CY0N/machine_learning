#!/usr/bin/env python
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor

import numpy as np
import common.feature_selection as feat_sel
import common.test_env as test_env

__author__ = 'A1CY0N'
__version__ = '0.1-dev'
__date__ = '7 october 2019'


def print_metrics(y_true, y_pred, label):
    from sklearn.metrics import r2_score
    # Feel free to extend it with additional metrics from sklearn.metrics
    print('%s R squared: %.2f' % (label, r2_score(y_true, y_pred)))


def linear_regression(X, y, print_text='Linear regression all in'):
    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    # Linear regression all in
    reg = LinearRegression()
    reg.fit(X_train, y_train)
    print_metrics(y_test, reg.predict(X_test), print_text)
    return reg


def linear_regression_selection(X, y):
    X_sel = feat_sel.backward_elimination(X, y)
    return linear_regression(X_sel, y, print_text='Linear regression with feature selection')

# STUDENT SHALL CREATE FUNCTIONS FOR POLYNOMIAL REGRESSION, SVR, DECISION TREE REGRESSION AND RANDOM FOREST REGRESSION


def polynomial_regression(X, y):
    poly_reg = PolynomialFeatures(degree=2)
    X_poly = poly_reg.fit_transform(X)
    return linear_regression(X_poly, y, print_text='Polynomial regression with feature selection')


def svr_regression(X, y, print_text='SVR regression'):
    sc = StandardScaler()
    X = sc.fit_transform(X)
    y = sc.fit_transform(np.expand_dims(y, axis=1))

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    svr_reg = SVR(kernel='rbf', gamma='auto')
    svr_reg.fit(X_train, np.ravel(y_train))
    # STUDENT SHALL ADD TEST-TRAIN SPLIT AND REGRESSOR CREATION AND TRAINING HERE
    print_metrics(np.squeeze(y_test), np.squeeze(
        svr_reg.predict(X_test)), 'SVR')
    return svr_reg


def decision_tree_regression(X, y, print_text='Decision tree regression'):

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    decision_tree_reg = DecisionTreeRegressor(
        max_depth=None, min_samples_split=2, min_samples_leaf=1, random_state=0)
    decision_tree_reg.fit(X_train, y_train)
    print_metrics(y_test, decision_tree_reg.predict(X_test), print_text)
    return decision_tree_reg


def random_forest_regression(X, y, print_text='Random forest regression'):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    random_forest_reg = RandomForestRegressor(n_estimators=10, random_state=0)
    random_forest_reg.fit(X_train, y_train)
    print_metrics(y_test, random_forest_reg.predict(X_test), print_text)


if __name__ == '__main__':
    REQUIRED = ['numpy', 'statsmodels', 'sklearn']
    test_env.versions(REQUIRED)

    # https://scikit-learn.org/stable/datasets/index.html#boston-house-prices-dataset
    X, y = load_boston(return_X_y=True)

    linear_regression(X, y)
    linear_regression_selection(X, y)
    polynomial_regression(X, y)
    svr_regression(X, y)
    decision_tree_regression(X, y)
    random_forest_regression(X, y)
    # STUDENT SHALL CALL POLYNOMIAL REGRESSION, SVR, DECISION TREE REGRESSION AND RANDOM FOREST REGRESSION FUNCTIONS

    print('Done')
